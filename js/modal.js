class cartItem {
  constructor(img, name,id, quantity, price, total) {
    this.img = img;
    this.name = name;
    this.id=id;
    this.quantity = quantity;
    this.price = price;
    this.total = () => {return this.price * this.quantity};
  }
}
