const BASE_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";

let showList = () => {
  axios({
    url: `${BASE_URL}/`,
    method: "GET",
  })
    .then(function (res) {
      console.log("yes", res.data);
      renderTable(res.data);
    })
    .catch(function (err) {
      console.log("no", err);
    });
};
showList();
// filter
document
  .getElementById("flexRadioDefault1")
  .addEventListener("change", function () {
    axios({
      url: `${BASE_URL}/`,
      method: "GET",
    })
      .then(function (res) {
        let product = res.data;
        let iphone = [];
        for (var i = 0; i < product.length; i++) {
          if (product[i].type == "Iphone") {
            iphone.push(product[i]);
            renderTable(iphone);
          }
        }
      })
      .catch(function (err) {
        console.log("no", err);
      });
  });
document
  .getElementById("flexRadioDefault2")
  .addEventListener("change", function () {
    axios({
      url: `${BASE_URL}/`,
      method: "GET",
    })
      .then(function (res) {
        let product = res.data;
        let samsung = [];
        for (var i = 0; i < product.length; i++) {
          if (product[i].type == "Samsung") {
            samsung.push(product[i]);
            renderTable(samsung);
          }
        }
      })
      .catch(function (err) {
        console.log("no", err);
      });
  });
document
  .getElementById("flexRadioDefault3")
  .addEventListener("change", function () {
    axios({
      url: `${BASE_URL}/`,
      method: "GET",
    })
      .then(function (res) {
        renderTable(res.data);
      })
      .catch(function (err) {
        console.log("no", err);
      });
  });
// show product list
let renderTable = (arr) => {
  var contentHTML = "";
  for (var i = 0; i < arr.length; i++) {
    var product = arr[i];
    var divContent = `
      <div class="col-md-12 col-lg-4 mb-4 mb-lg-0">
        <div class="card mb-3">
          <div class="d-flex justify-content-between p-3">
            <p class="lead mb-0">Mã sản phẩm số ${product.id} </p>
           
          </div>
          <img
            src="${product.img}"
            class="card-img-top"            
          />
          <div class="card-body">
          <div class="d-flex justify-content-between">
              <p class="small"><a href="#!" class="text-muted">${product.name}</a></p>
            </div>
            <div class="d-flex justify-content-between">
              <p class="small"><a href="#!" class="text-muted"> Hãng: ${product.type} </a></p>              
            </div>
	    <div class="d-flex justify-content-between">
              <p class="small"><a href="#!" class="text-muted">${product.desc}Màn hình ${product.screen}, camera trước ${product.frontCamera}, camera sau ${product.backCamera}</a></p>
              
            </div>

            <div class="d-flex justify-content-between mb-3">
              <h5 class="text-dark mb-0">Giá: ${product.price}$</h5>
              <button class="btn btn-success" onclick=addToCart(${product.id})> Thêm vào giỏ hàng</button>
            </div>

            
          </div>
        </div>
      </div>
      `;
    contentHTML += divContent;
  }
  document.getElementById("productTable").innerHTML = contentHTML;
};
// addToCart
let cart = [];

let addToCart = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then(function (res) {
      // plus one if already in cart
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
          cart[i].quantity += 1;
          renderCartTable(cart);
          return;
        }
      }
      let newItem = new cartItem(
        res.data.img,
        res.data.name,
        res.data.id,
        1,
        res.data.price
      );
      cart.push(newItem);
      renderCartTable(cart);
    })
    .catch(function (err) {
      console.log("err", err);
    });
};
// renderCartTable
let renderCartTable = (arr) => {
  let totalPayment = 0;
  var contentHTML = "";
  for (var i = 0; i < arr.length; i++) {
    var product = arr[i];
    var divContent = `
    <div class="row align-items-center pb-3">
                  <div class="col-md-2">
                    <img
                      src="${product.img}"
                      class="img-fluid"
                      alt="Generic placeholder image"
                    />
                  </div>
                  <div class="col-md-2 d-flex justify-content-center">
                    <div>
                      <p class="lead fw-normal mb-0">${
                        product.name
                      }</p>                                          
                    </div>
                  </div>

                  <div class="col-md-2 d-flex justify-content-center">
                  <button class="btn btn-link px-2" onclick=decrease(${
                    product.id
                  })>
                  <i class="fas fa-minus"></i>
                  </button>
                  
                  <input min="0" name="quantity" value="${
                    product.quantity
                  }" type="number" class="form-control form-control-sm" />
                  
                  <button class="btn btn-link px-2" onclick=increase(${
                    product.id
                  })>                       
                  <i class="fas fa-plus"></i>
                  </button>
                  </div>
                  <div class="col-md-2 d-flex justify-content-center">
                    <div>
                      <p class="lead fw-normal mb-0">${product.price}</p>
                    </div>
                  </div>
                  <div class="col-md-2 d-flex justify-content-center">
                    <div>
                      <p class="lead fw-normal mb-0">${product.total()}</p>
                    </div>
                  </div>
                  <div class="col-md-2 d-flex justify-content-center">
                    <div>
                      <a href="#!" class="text-danger" onclick=deleteCartItem(${
                        product.id
                      })
                        ><i class="fas fa-trash fa-lg"></i
                      ></a>
                    </div>
                  </div>
                </div>
    `;
    contentHTML += divContent;
    totalPayment += product.total();
  }
  document.getElementById("cartTable").innerHTML = contentHTML;
  document.getElementById("total-payment").innerHTML = `${totalPayment}$`;
};

// increase
let increase = (id) => {
  for (var i = 0; i < cart.length; i++) {
    if (cart[i].id == id) {
      cart[i].quantity += 1;
    }
  }
  renderCartTable(cart);
};
// decrease
let decrease = (id) => {
  for (var i = 0; i < cart.length; i++) {
    if (cart[i].id == id) {
      cart[i].quantity -= 1;
    }
  }
  renderCartTable(cart);
};
// delete
let deleteCartItem = (id) => {
  for (var i = 0; i < cart.length; i++) {
    if (cart[i].id == id) {
      cart.splice(i, 1);
    }
  }
  renderCartTable(cart);
};
// checkout
let checkOut = () => {
  cart = [];
  renderCartTable(cart);
  alert("Thanh toán thành công");
};
